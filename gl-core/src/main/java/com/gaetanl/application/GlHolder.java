package com.gaetanl.application;

import android.content.Context;
import android.support.annotation.NonNull;

import com.gaetanl.util.Callback;

/**
 * Holder interface aimed at holding application data. Initialize variables (ie. after a login)
 * using initialize(), reset them (ie. after a logout) using reset()
 *
 * GlHolder should be a lazy-loaded synchronized singleton insuring there is only one
 * instance of the application, constructor should be private and a static getInstance() method
 * should be used to retrieve the GlHolder instance
 *
 * @author Gaetan Leu
 */
public interface GlHolder {
    /**
     * Voids application data to insure security if needed (ie. after a logout)
     */
    public abstract void reset(@NonNull Context applicationContext);

    /**
     * Initializes application data (ie. after a login)
     */
    public abstract void initialize(@NonNull Context applicationContext, @NonNull Callback<Void> callback);
}
