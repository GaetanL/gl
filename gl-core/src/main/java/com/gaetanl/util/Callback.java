package com.gaetanl.util;

import android.support.annotation.NonNull;

/**
 * Callback retrieving data or error
 *
 * @author Gaetan Leu
 */
public interface Callback<T> {
    public void onResponse(T data);
    public void onError(@NonNull Throwable throwable);
}