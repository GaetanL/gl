package com.gaetanl.domain;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Base interface for elements with retrievable text
 *
 * @author Gaetan Leu
 */
public interface HasText {
    public @NonNull String getText(Context context);
}
