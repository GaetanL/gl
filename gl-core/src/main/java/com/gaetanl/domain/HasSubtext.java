package com.gaetanl.domain;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Base interface for elements with retrievable subtext
 *
 * @author Gaetan Leu
 */
public interface HasSubtext {
    public @NonNull String getSubtext(@NonNull Context context);
}
