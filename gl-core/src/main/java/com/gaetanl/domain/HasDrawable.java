package com.gaetanl.domain;

import android.support.annotation.DrawableRes;


/**
 * Base interface for elements with retrievable drawable resource
 * Returns DrawableRes instead of Drawable so that tint may be altered if needed
 *
 * @author Gaetan Leu
 */
public interface HasDrawable {
    public @DrawableRes int getDrawableResId();
}
