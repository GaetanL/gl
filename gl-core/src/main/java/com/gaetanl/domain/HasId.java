package com.gaetanl.domain;

import android.support.annotation.Nullable;

/**
 * Created by gaetan.leu on 09/06/2017.
 */

public interface HasId {
    public @Nullable Long getId();
}
