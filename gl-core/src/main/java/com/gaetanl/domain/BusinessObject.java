package com.gaetanl.domain;

import android.support.annotation.Nullable;

/**
 * Base class for business objects
 *
 * @author Gaetan Leu
 */
public abstract class BusinessObject implements HasId {
    protected @Nullable Long id;

    public BusinessObject(@Nullable Long id) {
        this.id = id;
    }

    @Override
    public @Nullable Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("BusinessObject#%d", id);
    }
}
