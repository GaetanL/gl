package com.gaetanl.domain;

import android.support.annotation.NonNull;
import android.util.Log;

import com.gaetanl.util.Callback;

/**
 * Lazy-loaded object, holding a local value initialized when needed
 *
 * @author Gaetan Leu
 */
public abstract class Lazy<T> {
    private T localObject = null;

    /**
     * Returns the localObject straight away if it's not null and if it's upToDate,
     * otherwise calls update() to refresh localObject and recalls itself
     *
     * @param callback
     */
    public void get(@NonNull final Callback<T> callback) {
        if (getLocalObject() != null && isLocalObjectUpToDate()) {
            Log.d("DEBUG", "up to date local value"); // TODO remove
            callback.onResponse(getLocalObject());
        }
        else {
            Log.d("DEBUG", "outdated local value"); // TODO remove
            update(new Callback<Void>() {@Override
                public void onResponse(Void data) {
                    get(callback);
                }

                @Override
                public void onError(Throwable throwable) {
                    callback.onError(throwable);
                }
            });
        }
    }

    /**
     * Updates localObject, should use setLocalObject() on success
     *
     * @param callback
     */
    protected abstract void update(@NonNull Callback<Void> callback);

    /**
     * Returns the localObject
     *
     * @return the local value
     */
    protected T getLocalObject() {
        return localObject;
    }

    /**
     * Sets the localObject and upToDate flag to true
     *
     * @param localObject the new local value
     */
    protected void setLocalObject(@NonNull T localObject) {
        this.localObject = localObject;
    }

    /**
     * Returns true if the localObject is up to date
     *
     * @return true the localObject is up to date
     */
    protected abstract boolean isLocalObjectUpToDate();

    /**
     * Declares the localObject obsolete, indicating that next query to get() will need to update()
     * the object first, without having to reloading it straight away. This function should be used
     * every time an object is updated and the localObject is not relevant anymore
     */
    public abstract void setLocalObjectObsolete();
}
