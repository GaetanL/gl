package com.gaetanl.presentation;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toolbar;

import com.gaetanl.domain.HasDrawable;
import com.gaetanl.domain.HasText;
import com.gaetanl.gl.core.R;
import com.gaetanl.presentation.adapter.GenericAdapter;

import java.util.ArrayList;
import java.util.List;

import static android.view.MenuItem.SHOW_AS_ACTION_IF_ROOM;

/**
 * Helper class to build UI elements (popups, menus, etc.)
 *
 * @author Gaetan Leu
 */
public class UiBuilder {
    private final Activity context;

    public UiBuilder(@NonNull Activity context) {
        this.context = context;
    }

    public void buildDrawerMenu(@IdRes int listViewId, @NonNull ArrayList<HasText> menu) {
        setAdapter(listViewId, buildAdapter(menu));
    }

    public void buildDrawerMenu(@IdRes int listViewId, @LayoutRes int itemLayoutResId, @LayoutRes int sectionLayoutResId, @NonNull ArrayList<HasText> menu) {
        setAdapter(listViewId, buildAdapter(itemLayoutResId, sectionLayoutResId, menu));
    }

    private ArrayAdapter buildAdapter(@NonNull ArrayList<HasText> menu) {
        return new GenericAdapter(context, menu);
    }

    private ArrayAdapter buildAdapter(@LayoutRes int itemLayoutResId, @LayoutRes int sectionLayoutResId, @NonNull ArrayList<HasText> menu) {
        GenericAdapter adapter = new GenericAdapter(context, menu);
        adapter.setItemLayoutResId(itemLayoutResId);
        adapter.setSectionLayoutResId(sectionLayoutResId);
        return adapter;
    }

    private void setAdapter(@IdRes int listViewId, ArrayAdapter adapter) {
        ((ListView) context.findViewById(listViewId)).setAdapter(adapter);
    }

    public void buildToolbar(@IdRes int toolbarViewId) {
        Toolbar toolbar = (Toolbar) context.findViewById(toolbarViewId);
        GraphicsBuilder builder = new GraphicsBuilder(context);
        toolbar.setElevation(builder.unitToPixels(TypedValue.COMPLEX_UNIT_DIP, 4));
        context.setActionBar(toolbar);
    }

    public void buildToolbar(@IdRes int toolbarViewId, @IdRes int drawerViewId) {
        buildToolbar(toolbarViewId);
        DrawerLayout drawerLayout = (DrawerLayout) context.findViewById(drawerViewId);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(context, drawerLayout, R.string.drawer_open, R.string.drawer_closed);
        drawerLayout.addDrawerListener(drawerToggle);
        ActionBar actionBar = context.getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        drawerToggle.syncState();
    }

    public void buildToolbarOptionMenu(Menu menu, List<HasText> options, @StyleRes int toolbarStyle) {
        menu.clear();
        if (options != null) {
            GraphicsBuilder builder = new GraphicsBuilder(context);
            for (int i = 0 ; i < options.size() ; i++) {
                HasText entry = options.get(i);
                MenuItem menuItem = menu.add(Menu.NONE, i, i, entry.getText(context));
                if (entry instanceof HasDrawable) {
                    @DrawableRes int drawableRes = ((HasDrawable) entry).getDrawableResId();
                    int[] attrs = {android.R.attr.colorControlNormal};
                    TypedArray ta = context.obtainStyledAttributes(toolbarStyle, attrs);
                    @ColorInt int iconColor = ta.getColor(0, Color.BLACK);
                    ta.recycle();
                    menuItem.setIcon(builder.getTintedDrawable(drawableRes, iconColor));
                    menuItem.setShowAsAction(SHOW_AS_ACTION_IF_ROOM);
                }
            }
        }
    }
}
