package com.gaetanl.presentation.adapter;

import com.gaetanl.domain.HasText;

/**
 * Base interface for list section
 *
 * @author Gaetan Leu
 */
public interface Section extends HasText {
}
