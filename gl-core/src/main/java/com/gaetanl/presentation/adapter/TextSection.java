package com.gaetanl.presentation.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

/**
 * List section with text
 *
 * @author Gaetan Leu
 */
public class TextSection extends TextItem implements Section {
    public TextSection(@StringRes int textResId) {
        super(textResId);
    }

    public TextSection(@NonNull String hardCodedText) {
        super(hardCodedText);
    }
}
