package com.gaetanl.presentation.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.gaetanl.domain.HasText;

/**
 * List item with text
 *
 * @author Gaetan Leu
 */
public class TextItem implements HasText {
    private final @StringRes Integer textResId;
    private final String hardCodedText;

    public TextItem(@StringRes int textResId) {
        this.textResId = textResId;
        this.hardCodedText = null;
    }

    public TextItem(@NonNull String hardCodedText) {
        this.textResId = null;
        this.hardCodedText = hardCodedText;
    }

    @Override
    public @NonNull String getText(@NonNull Context context) {
        String text = null;

        if (textResId != null) {
            text = context.getString(textResId);
        }
        else if (hardCodedText != null) {
            text = hardCodedText;
        }

        return text; // TODO check null
    }
}
