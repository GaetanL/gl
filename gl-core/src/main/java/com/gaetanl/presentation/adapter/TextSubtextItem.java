package com.gaetanl.presentation.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.gaetanl.domain.HasSubtext;

/**
 * List item with text and subtext
 *
 * @author Gaetan Leu
 */
public class TextSubtextItem extends TextItem implements HasSubtext {
    private final @StringRes Integer subtextResId;
    private final String hardCodedSubtext;

    public TextSubtextItem(@StringRes int textResId, @StringRes int subtextResId) {
        super(textResId);
        this.subtextResId = subtextResId;
        this.hardCodedSubtext = null;
    }

    public TextSubtextItem(@StringRes int textResId, @NonNull String hardCodedSubtext) {
        super(textResId);
        this.subtextResId = null;
        this.hardCodedSubtext = hardCodedSubtext;
    }

    @Override
    public @NonNull String getSubtext(@NonNull Context context) {
        String subtext = null;

        if (subtextResId != null) {
            subtext = context.getString(subtextResId);
        }
        else if (hardCodedSubtext != null) {
            subtext = hardCodedSubtext;
        }

        return subtext; // TODO check null
    }
}
