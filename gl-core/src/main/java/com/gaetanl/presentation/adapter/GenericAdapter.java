package com.gaetanl.presentation.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.gaetanl.domain.HasDrawable;
import com.gaetanl.domain.HasSubtext;
import com.gaetanl.domain.HasText;
import com.gaetanl.gl.core.R;
import com.gaetanl.presentation.GraphicsBuilder;
import com.gaetanl.presentation.SwitchableModeListView;

import java.util.ArrayList;

import static android.view.View.GONE;
import static com.gaetanl.presentation.SwitchableModeListView.Mode.SELECTION;

/**
 * Generic adapter that takes as parameter list of items implementing at least HasText, with
 * specific treatment for items implementing Section, HasDrawable and HasSubtext
 *
 * @author Gaetan Leu
 */
public class GenericAdapter extends ArrayAdapter<HasText> {
    private SwitchableModeListView listView = null;
    private @LayoutRes int itemLayoutResId;
    private @LayoutRes int sectionLayoutResId;
    private final boolean containsDrawable;

    public GenericAdapter(@NonNull Context context, @NonNull ArrayList<HasText> objects) {
        super(context, R.layout.list_item, objects);
        this.itemLayoutResId = R.layout.list_item;
        this.sectionLayoutResId = R.layout.list_section;

        for (HasText object: objects) {
            if (object instanceof HasDrawable) {
                containsDrawable = true;
                return;
            }
        }
        containsDrawable = false;
    }

    public void setItemLayoutResId(@LayoutRes int itemLayoutResId) {
        this.itemLayoutResId = itemLayoutResId;
    }

    public void setSectionLayoutResId(@LayoutRes int sectionLayoutResId) {
        this.sectionLayoutResId = sectionLayoutResId;
    }

    public void setSwitchableListView(@NonNull SwitchableModeListView listView) {
        this.listView = listView;
    }

    /**
     * Holder for views to be populated by the adapter
     */
    private class ViewHolder {
        TextView textView;
        ImageView imageView = null;
        TextView subtextView = null;
        CheckBox checkboxView = null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        HasText entry = getItem(position);
        boolean isSection = entry instanceof Section;
        boolean hasDrawable = entry instanceof HasDrawable;
        boolean hasSubtext = entry instanceof HasSubtext;

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(android.app.Activity.LAYOUT_INFLATER_SERVICE);
        @LayoutRes int layoutResId = isSection ? sectionLayoutResId : itemLayoutResId;
        convertView = layoutInflater.inflate(layoutResId, null);
        viewHolder = new ViewHolder();
        viewHolder.textView = (TextView) convertView.findViewById(R.id.text);
        viewHolder.imageView = (ImageView) convertView.findViewById(R.id.drawable);
        viewHolder.subtextView = (TextView) convertView.findViewById(R.id.subtext);
        viewHolder.checkboxView = (CheckBox) convertView.findViewById(R.id.checkbox);
        convertView.setTag(viewHolder);

        // Text
        viewHolder.textView.setText(entry.getText(getContext()));

        // Drawable
        if (hasDrawable && viewHolder.imageView != null) {
            GraphicsBuilder builder = new GraphicsBuilder(getContext());
            Drawable icon = builder.getTintedDrawable(((HasDrawable) entry).getDrawableResId());
            viewHolder.imageView.setImageDrawable(icon);
        }
        else if (!containsDrawable && viewHolder.imageView != null) {
            viewHolder.imageView.setVisibility(GONE);
            View separator = convertView.findViewById(R.id.separator);
            if (separator != null) {
                separator.setVisibility(GONE);
            }
        }

        // Subtext
        if (hasSubtext && viewHolder.subtextView != null) {
            viewHolder.subtextView.setText(((HasSubtext) entry).getSubtext(getContext()));
        }
        else if (viewHolder.subtextView != null) {
            viewHolder.subtextView.setVisibility(GONE);
        }

        // Checkbox
        if (viewHolder.checkboxView != null && listView != null && listView.getSelectedMap() != null) {
            viewHolder.checkboxView.setChecked(listView.getSelectedMap().get(position));
            viewHolder.checkboxView.setVisibility(SELECTION.equals(listView.getMode()) ? View.VISIBLE : View.INVISIBLE);
        }

        return convertView;
    }
}
