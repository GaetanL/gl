package com.gaetanl.presentation.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gaetanl.domain.BusinessObject;
import com.gaetanl.domain.HasText;
import com.gaetanl.gl.core.R;
import com.gaetanl.presentation.SwitchableModeListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Helper class for ListView's alphabetical side index functions
 *
 * @author Gaetan Leu
 */
public class AlphaSideIndexManager<T extends BusinessObject & HasText> {
    private final Context context;
    private final Class<T> objectClass;
    private List<T> localObjects;
    private final Comparator comparator;
    private SwitchableModeListView listView;
    private final LinearLayout sideIndex;
    private int indexListSize;
    private int sideIndexHeight;
    private float sideIndexX;
    private float sideIndexY;

    private ArrayList<HasText> rows;
    private List<Object[]> alphabet;
    private HashMap<String, Integer> sections;

    public class SideIndexGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            // we know already coordinates of first touch
            // we know as well a scroll distance
            sideIndexX = sideIndexX - distanceX;
            sideIndexY = sideIndexY - distanceY;

            // when the user scrolls within our side index
            // we can show for every position in it a proper
            // item in the country list
            if (sideIndexX >= 0 && sideIndexY >= 0) {
                displayListItem();
            }

            return super.onScroll(e1, e2, distanceX, distanceY);
        }
    }

    public AlphaSideIndexManager(Context context, Class<T> objectClass, List<T> localObjects, Comparator comparator,
                                 View rootView, LinearLayout sideIndex) {
        this.context = context;
        this.objectClass = objectClass;
        this.localObjects = localObjects;
        this.comparator = comparator;
        this.sideIndex = sideIndex;

        final GestureDetector gestureDetector = new GestureDetector(context, new SideIndexGestureListener());
        rootView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });
    }

    public T get(int index) {
        T object = null;
        HasText row = rows.get(index);
        if (row.getClass().equals(objectClass)) {
            object = (T) row;
        }
        return object;
    }

    public void setSwitchableListView(@NonNull SwitchableModeListView listView) {
        this.listView = listView;
    }

    public void updateList(Collection<T> objects) {
        if (localObjects == null || !localObjects.equals(objects)) {
            localObjects = new ArrayList<T>(objects);
            alphabet = new ArrayList<Object[]>();
            sections = new HashMap<String, Integer>();
            Collections.sort(localObjects, comparator);
            rows = new ArrayList<>();

            int start = 0;
            int end = 0;
            String previousLetter = null;
            Object[] tmpIndexItem = null;
            Pattern numberPattern = Pattern.compile("[0-9]");

            for (T object: localObjects) {
                String firstLetter = object.getText(context).substring(0, 1).toUpperCase();

                // Group numbers together in the scroller
                if (numberPattern.matcher(firstLetter).matches()) {
                    firstLetter = "#";
                }

                // If we've changed to a new letter, add the previous letter to the alphabet scroller
                if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                    end = rows.size() - 1;
                    tmpIndexItem = new Object[3];
                    tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
                    tmpIndexItem[1] = start;
                    tmpIndexItem[2] = end;
                    alphabet.add(tmpIndexItem);

                    start = end + 1;
                }

                // Check if we need to add a header row
                if (!firstLetter.equals(previousLetter)) {
                    rows.add(new TextSection(firstLetter));
                    sections.put(firstLetter, start);
                }

                // Add the country to the list
                rows.add(object);
                previousLetter = firstLetter;
            }

            if (previousLetter != null) {
                // Save the last letter
                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = rows.size() - 1;
                alphabet.add(tmpIndexItem);
            }

            GenericAdapter adapter = new GenericAdapter(context, rows);
            if (listView != null && listView.getSelectedMap() != null) {
                listView.getSelectedMap().clear();
                // Reinitialize "selected" array indicating which rows are selected,
                // list has been updated so unselect everything
                for (int i = 0 ; i < rows.size() ; i++) {
                    listView.getSelectedMap().add(rows.get(i)instanceof Section ? null : false); // Sections are not selectable
                }
                adapter.setSwitchableListView(listView);
            }
            listView.setAdapter(adapter);

            updateSideIndex();
        }
    }

    private void updateSideIndex() {
        sideIndex.removeAllViews();
        indexListSize = alphabet.size();
        if (indexListSize < 1) {
            return;
        }

        int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 20);
        int tmpIndexListSize = indexListSize;
        while (tmpIndexListSize > indexMaxSize) {
            tmpIndexListSize = tmpIndexListSize / 2;
        }
        double delta;
        if (tmpIndexListSize > 0) {
            delta = indexListSize / tmpIndexListSize;
        } else {
            delta = 1;
        }

        TextView tmpTV;
        for (double i = 1; i <= indexListSize; i = i + delta) {
            Object[] tmpIndexItem = alphabet.get((int) i - 1);
            String tmpLetter = tmpIndexItem[0].toString();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            tmpTV = (TextView) inflater.inflate(R.layout.slider_letter, sideIndex, false);
            tmpTV.setText(tmpLetter);
            sideIndex.addView(tmpTV);
        }

        sideIndexHeight = sideIndex.getHeight();

        sideIndex.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                sideIndexX = event.getX();
                sideIndexY = event.getY();

                // and can display a proper item it country list
                displayListItem();

                return false;
            }
        });
    }

    private void displayListItem() {
        sideIndexHeight = sideIndex.getHeight();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

        // get the item (we can do it since we know item index)
        if (itemPosition < alphabet.size()) {
            Object[] indexItem = alphabet.get(itemPosition);
            int subitemPosition = sections.get(indexItem[0]);

            listView.setSelection(subitemPosition);
        }
    }
}
