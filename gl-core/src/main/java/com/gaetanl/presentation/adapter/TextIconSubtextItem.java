package com.gaetanl.presentation.adapter;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.gaetanl.domain.HasSubtext;

/**
 * List item with text, icon and subtext
 *
 * @author Gaetan Leu
 */
public class TextIconSubtextItem extends TextIconItem implements HasSubtext {
    private final @StringRes Integer subtextResId;
    private final String hardCodedSubtext;

    public TextIconSubtextItem(@StringRes int textResId, @DrawableRes int drawableResId, @StringRes int subtextResId) {
        super(textResId, drawableResId);
        this.subtextResId = subtextResId;
        this.hardCodedSubtext = null;
    }

    public TextIconSubtextItem(@StringRes int textResId, @DrawableRes int drawableResId, String hardCodedSubtext) {
        super(textResId, drawableResId);
        this.subtextResId = null;
        this.hardCodedSubtext = hardCodedSubtext;
    }

    @Override
    public @NonNull String getSubtext(@NonNull Context context) {
        String subtext = null;

        if (subtextResId != null) {
            subtext = context.getString(subtextResId);
        }
        else if (hardCodedSubtext != null) {
            subtext = hardCodedSubtext;
        }

        return subtext; // TODO check null
    }
}
