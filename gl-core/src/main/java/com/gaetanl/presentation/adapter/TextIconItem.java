package com.gaetanl.presentation.adapter;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.gaetanl.domain.HasDrawable;

/**
 * List item with text and icon
 *
 * @author Gaetan Leu
 */
public class TextIconItem extends TextItem implements HasDrawable {
    private final @DrawableRes int drawableResId;

    public TextIconItem(@StringRes int textResId, @DrawableRes int drawableResId) {
        super(textResId);
        this.drawableResId = drawableResId;
    }

    @Override
    public int getDrawableResId() {
        return drawableResId;
    }
}