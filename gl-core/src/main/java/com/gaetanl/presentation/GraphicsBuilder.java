package com.gaetanl.presentation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.gaetanl.gl.core.R;

/**
 * Helper related to graphics (ie. Drawable) operations
 *
 * @author Gaetan Leu
 */
public class GraphicsBuilder {
    private final Context context;

    public GraphicsBuilder(@NonNull Context context) {
        this.context = context;
    }

    /**
     * Retrieves a resource from context's theme using the resource id
     *
     * @param resId the resource id, ie. android.R.attr.colorPrimary
     * @return the resource value, or null if not found
     */
    public @Nullable Integer getResourceFromTheme(int resId) {
        Integer result = null;

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        if (theme.resolveAttribute(resId, typedValue, true)) {
            result = typedValue.data;
        }

        return result;
    }

    /**
     * Retrieves a resource from context's theme using the resource name
     *
     * @param resName the resource name, ie. "colorPrimary"
     * @return the resource value, or null if not found
     */
    public @Nullable Integer getResourceFromTheme(@NonNull String resName) {
        int resId = context.getResources().getIdentifier(resName, "attr", context.getPackageName());
        return getResourceFromTheme(resId);
    }

    /**
     * Returns a drawable, if drawable resource name starts with the prefix "icon_", tints it with
     * the context theme iconColor, if available, otherwise returns the raw drawable
     * @param drawableRes id of the drawable resource
     * @return the drawable, tinted if possible, raw if not
     */
    public @Nullable Drawable getTintedDrawable(@DrawableRes int drawableRes) {
        String drawableResName = context.getResources().getResourceEntryName(drawableRes);
        Integer colorInt = getResourceFromTheme(R.attr.iconColor);
        if (drawableResName != null && colorInt != null && drawableResName.startsWith("icon_")) {
            return getTintedDrawable(drawableRes, colorInt);
        }
        else {
            return ContextCompat.getDrawable(context, drawableRes);
        }
    }

    /**
     * Returns a drawable, tinted with the specified color
     * @param drawableRes id of the drawable resource
     * @param colorInt color to tint the drawable with
     * @return the tinted drawable
     */
    public @Nullable Drawable getTintedDrawable(@DrawableRes int drawableRes, @ColorInt int colorInt) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableRes);
        drawable.setTint(colorInt);
        return drawable;
    }

    /**
     * Converts a complex unit measurement in pixels depending on the context
     * @param unit of the measurement, possible values:
     *             TypedValue.COMPLEX_UNIT_PX,
     *             TypedValue.COMPLEX_UNIT_DIP,
     *             TypedValue.COMPLEX_UNIT_SP,
     *             TypedValue.COMPLEX_UNIT_PT,
     *             TypedValue.COMPLEX_UNIT_IN,
     *             TypedValue.COMPLEX_UNIT_MM
     * @param value of the measurement
     * @return the measurements converted in context-dependant pixels
     */
    public float unitToPixels(@IntRange(from = TypedValue.COMPLEX_UNIT_PX, to = TypedValue.COMPLEX_UNIT_MM) int unit, float value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(unit, value, metrics);
    }
}
