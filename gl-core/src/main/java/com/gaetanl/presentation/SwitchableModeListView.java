package com.gaetanl.presentation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import com.gaetanl.gl.core.R;

import java.util.ArrayList;

import static com.gaetanl.presentation.SwitchableModeListView.Mode.DISPLAY;
import static com.gaetanl.presentation.SwitchableModeListView.Mode.SELECTION;

/**
 * ListView that can switch from default display mode to selection mode with a long click, in
 * display mode the standard onItemClick behavior is defined by using setDisplayModeListener, while
 * in selection mode, a click on a row selects it, a long press switches again to display mode,
 * unselecting all rows in the process
 *
 * @author Gaetan Leu
 */
public class SwitchableModeListView extends ListView {
    public enum Mode {
        DISPLAY,
        SELECTION
    }
    private Mode mode = Mode.DISPLAY;
    private ArrayList<Boolean> selectedMap = null;

    public class DefaultOnItemLongClickListener implements OnItemLongClickListener {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            setMode(SELECTION.equals(mode) ? DISPLAY : SELECTION);
            if (SELECTION.equals(mode)) {
                selectItem(view, position);
            }
            return true;
        }
    }

    public class DefaultOnItemClickListener implements OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(view, position);
        }
    }

    private OnItemClickListener displayModeListener = null;
    private OnItemClickListener selectionModeListener = new DefaultOnItemClickListener();

    public SwitchableModeListView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.listViewStyle);
    }

    public SwitchableModeListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mode = DISPLAY;
        setOnItemLongClickListener(new DefaultOnItemLongClickListener());

        setOnItemClickListener(displayModeListener);
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        for (int i = 0 ; i < selectedMap.size() ; i++) {
            View rowView = getChildAt(i);
            try {
                CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.checkbox);
                checkBox.setVisibility(SELECTION.equals(mode) ? View.VISIBLE : View.INVISIBLE);

                // if switching mode, unselect all rows
                checkBox.setChecked(false);
            }
            catch (Exception e) {}
            finally {
                selectedMap.set(i, selectedMap.get(i) == null ? null : false);
            }
        }
        this.mode = mode;
        updateOnItemClickListener();
    }

    public ArrayList<Boolean> getSelectedMap() {
        return selectedMap;
    }

    public void setSelectedMap(ArrayList<Boolean> selectedMap) {
        this.selectedMap = selectedMap;
    }

    public void setDisplayModeListener(OnItemClickListener displayModeListener) {
        this.displayModeListener = displayModeListener;
        updateOnItemClickListener();
    }

    private void updateOnItemClickListener() {
        setOnItemClickListener(SELECTION.equals(mode) ? selectionModeListener : displayModeListener);
    }

    private void selectItem(@NonNull View view, int position) {
        try {
            Boolean selected = selectedMap.get(position);
            if (selected != null) {
                selectedMap.set(position, !selected);
                CheckBox checkbox;
                checkbox = (CheckBox) view.findViewById(R.id.checkbox);
                checkbox.setChecked(!selected);
            }
        }
        finally {}
    }
}
