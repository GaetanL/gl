package com.gaetanl.persistence;

import android.support.annotation.NonNull;

import com.gaetanl.domain.BusinessObject;

import java.util.Map;

/**
 * Helper class to factorize code retrieving the value of a BusinessObject instance's fields
 *
 * @author Gaetan Leu
 */
public interface FieldsValueMapper<T extends BusinessObject> {
    /**
     * Maps the fields of a BusinessObject class and their values in the specified instance
     * @param object the object whose values are to be mapped
     * @return the map of fields-values
     */
    public @NonNull Map<String, Object> map(T object);
}
