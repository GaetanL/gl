package com.gaetanl.persistence;

import com.gaetanl.domain.BusinessObject;

/**
 * Dao for JSON persistence
 */
public abstract class JsonDao<T extends BusinessObject> implements Dao<T> {
}
