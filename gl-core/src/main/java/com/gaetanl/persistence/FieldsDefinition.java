package com.gaetanl.persistence;

import android.support.annotation.NonNull;

import com.gaetanl.domain.BusinessObject;

import java.util.Map;

/**
 * Helper class to factorize code retrieving the fields of a BusinessObject class
 *
 * @author Gaetan Leu
 */
public interface FieldsDefinition<T extends BusinessObject> {
    /**
     * Maps the fields of a BusinessObject class and their types
     * @return the mapping of fields and their type
     */
    public @NonNull Map<String, FieldType> get();
}
