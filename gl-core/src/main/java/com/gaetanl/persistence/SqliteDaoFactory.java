package com.gaetanl.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.gaetanl.domain.BusinessObject;

/**
 * Factory instanciating SqliteDao
 *
 * @author Gaetan Leu
 */
public abstract class SqliteDaoFactory extends SQLiteOpenHelper implements DaoFactory<SqliteDao> {
    protected final Context context;
    protected final String databaseName;
    protected final int databaseVersion;

    public SqliteDaoFactory(@NonNull Context context, @NonNull String databaseName, int databaseVersion) {
        super(context, databaseName, null, databaseVersion);
        this.context = context;
        this.databaseName = databaseName;
        this.databaseVersion = databaseVersion;
    }

    @Override
    public abstract void onCreate(SQLiteDatabase db);

    @Override
    public abstract void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion);

    @Override
    public @NonNull SqliteDao getDao(@NonNull Class<? extends BusinessObject> objectClass) {
        return getSqliteDao(objectClass);
    }

    protected abstract @NonNull SqliteDao<? extends BusinessObject> getSqliteDao(@NonNull Class<? extends BusinessObject> objectClass);
}
