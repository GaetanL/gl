package com.gaetanl.persistence;

import android.content.ContentValues;
import android.support.annotation.NonNull;

/**
 * Type of field for persistence purpose (ie.: transcribing to a database)
 *
 * @author Gaetan Leu
 */
public enum FieldType {
    ID {
        @Override
        public void putContentValue(@NonNull ContentValues contentValues, @NonNull String fieldName, @NonNull Object value) {
            contentValues.put(fieldName, (Integer) value);
        }
    },
    BOOLEAN {
        @Override
        public void putContentValue(@NonNull ContentValues contentValues, @NonNull String fieldName, @NonNull Object value) {
            contentValues.put(fieldName, (Boolean) value);
        }
    },
    INTEGER {
        @Override
        public void putContentValue(@NonNull ContentValues contentValues, @NonNull String fieldName, @NonNull Object value) {
            contentValues.put(fieldName, (Integer) value);
        }
    },
    TEXT {
        @Override
        public void putContentValue(@NonNull ContentValues contentValues, @NonNull String fieldName, @NonNull Object value) {
            contentValues.put(fieldName, (String) value);
        }
    };

    /**
     * Puts a field name and properly casted value inside a ContentValue object
     * @param contentValues the ContentValue object that will be used to put the name-value
     * @param fieldName the field name
     * @param value the instance value for this field
     */
    public abstract void putContentValue(@NonNull ContentValues contentValues, @NonNull String fieldName, @NonNull Object value);
}
