package com.gaetanl.persistence;

import android.database.sqlite.SQLiteOpenHelper;

import com.gaetanl.domain.BusinessObject;

/**
 * Dao for SQLite persistence
 */
public abstract class SqliteDao<T extends BusinessObject> implements Dao<T> {
    protected final SQLiteOpenHelper dbHandler;

    public SqliteDao(SQLiteOpenHelper dbHandler) {
        super();
        this.dbHandler = dbHandler;
    }
}
