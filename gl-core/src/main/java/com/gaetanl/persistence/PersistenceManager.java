package com.gaetanl.persistence;

import android.content.Context;
import android.support.annotation.NonNull;

import com.gaetanl.domain.BusinessObject;
import com.gaetanl.domain.Lazy;
import com.gaetanl.util.Callback;

import java.util.Collection;

/**
 * Manager for persistence operations
 *
 * @author Gaetan Leu
 */
public abstract class PersistenceManager {
    protected final Context context;
    protected final DaoFactory factory;

    public PersistenceManager(@NonNull Context context, @NonNull DaoFactory factory) {
        this.context = context;
        this.factory = factory;
    }

    public <T extends BusinessObject> void create(@NonNull T object, @NonNull Callback<Long> callback) {
        ((Dao<T>) factory.getDao(object.getClass())).create(object, callback);
    }

    public <T extends BusinessObject> void read(@NonNull Class<T> objectClass, long  id, @NonNull Callback<T> callback) {
        ((Dao<T>) factory.getDao(objectClass)).read(id, callback);
    }

    public <T extends BusinessObject> void readAll(@NonNull Class<T> objectClass, @NonNull Callback<Collection<T>> callback) {
        ((Dao<T>) factory.getDao(objectClass)).readAll(callback);
    }

    public <T extends BusinessObject> void update(@NonNull T object, @NonNull Callback<Void> callback) {
        ((Dao<T>) factory.getDao(object.getClass())).update(object, callback);
    }

    public <T extends BusinessObject> void delete(@NonNull T object, @NonNull Callback<Void> callback) {
        ((Dao<T>) factory.getDao(object.getClass())).delete(object, callback);
    }

    public @NonNull <T extends BusinessObject> Lazy<T> getLazyInstance(@NonNull Class<T> objectClass, final long id) {
        return ((Dao<T>) factory.getDao(objectClass)).getLazyInstance(id);
    }

    public @NonNull <T extends BusinessObject> Lazy<Collection<T>> getLazyCollectionInstance(@NonNull Class<T> objectClass) {
        return ((Dao<T>) factory.getDao(objectClass)).getLazyCollectionInstance();
    }
}
