package com.gaetanl.persistence;

import android.support.annotation.NonNull;

import com.gaetanl.domain.BusinessObject;

/**
 * Generic DaoFactory
 *
 * @author Gaetan Leu
 */
public interface DaoFactory<D extends Dao> {
    /**
     * Returns a Dao depending on the specified class of BusinessObject
     * @param objectClass the class of BusinessObject to manipulate
     * @return a Dao to manipulate T objects
     */
    public @NonNull D getDao(Class<? extends BusinessObject> objectClass);
}
