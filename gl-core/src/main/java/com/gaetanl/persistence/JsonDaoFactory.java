package com.gaetanl.persistence;

import android.support.annotation.NonNull;

import com.gaetanl.domain.BusinessObject;

/**
 * Factory instanciating JsonDao
 *
 * @author Gaetan Leu
 */
public abstract class JsonDaoFactory implements DaoFactory<JsonDao> {
    @Override
    public @NonNull JsonDao getDao(Class<? extends BusinessObject> objectClass) {
        return getJsonDao(objectClass);
    }

    protected abstract @NonNull JsonDao<? extends BusinessObject> getJsonDao(@NonNull Class<? extends BusinessObject> objectClass);
}
