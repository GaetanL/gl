package com.gaetanl.persistence;

import android.support.annotation.NonNull;

import com.gaetanl.domain.BusinessObject;
import com.gaetanl.domain.Lazy;
import com.gaetanl.util.Callback;

import java.util.Collection;
import java.util.Map;

/**
 * Generic Dao for T extends {@link BusinessObject}
 *
 * @author Gaetan Leu
 */
public interface Dao<T extends BusinessObject> {
    /**
     * Returns a map of fields relevant for persistence, as well as their simplified type (ie. text,
     * integer, etc.) that will be used by the concrete DAO to persist data
     * @return a map of relevant fields with their type
     */
    public @NonNull Map<String, FieldType> getFields();

    /**
     * Maps fields vith their value for a given object
     * @param object the object to map
     * @return a map of fields-value pairs
     */
    public @NonNull Map<String, Object> map(@NonNull T object);

    /**
     * Returns the name of this entity (the class of the business object), that will be used by the
     * DAO to persist data
     * @return the name of this entity
     */
    public abstract @NonNull String getEntityName();

    /**
     * Creates the business object
     * @param object the business object to persist
     * @param callback returns the id of the created object on success
     */
    public void create(@NonNull T object, @NonNull Callback<Long> callback);

    /**
     * Retrieves the business object with its id
     * @param id of the object to read
     * @param callback returns the object on success
     */
    public void read(long id, @NonNull Callback<T> callback);

    /**
     * Retrieves all the business objects of this type
     * @param callback returns all the business objects on success
     */
    public void readAll(@NonNull Callback<Collection<T>> callback);

    /**
     * Updates the business object
     * @param object to update
     * @param callback returns nothing on success
     */
    public void update(@NonNull T object, @NonNull Callback<Void> callback);

    /**
     * Deletes the business object
     * @param object to delete
     * @param callback returns nothing on success
     */
    public void delete(@NonNull T object, @NonNull Callback<Void> callback);

    /**
     * Returns a Lazy wrapper for T objects
     * @return Lazy wrapper for T objects
     */
    public @NonNull Lazy<T> getLazyInstance(final long id);

    /**
     * Returns a Lazy wrapper for collections of T objects
     * @return Lazy wrapper for collections of T objects
     */
    public @NonNull Lazy<Collection<T>> getLazyCollectionInstance();
}
